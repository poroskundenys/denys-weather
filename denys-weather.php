<?php
/**
 * Plugin Name: Denys weather
 * Description: Denys weather
 * Version: 1
 * Author:      Poroskun Denys
 * Text Domain: weather
 * Domain Path: /languages/
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class DenysWeather {

    private static $instance = null;

    public static function instance() {
        if ( null == self::$instance ) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function __construct() {

        if ( is_admin() ) {
            $this->admin_init();
        }
    }

    public function admin_init() {
        add_action( 'plugins_loaded', array( $this, 'load_plugin_textdomain' ), 99 );
        add_filter( 'plugin_action_links', array( $this, 'action_links' ) );
        add_action( 'admin_menu', array( $this, 'admin_menu_add_external_links_as_submenu' ) );
        add_action( 'admin_init', array( $this, 'plugin_settings' ) );
        add_filter( 'cron_schedules', array( $this,'three_hour') ); 
        if( !wp_next_scheduled('denys_send_email') ){
            wp_schedule_event( time(), 'three_hour', 'event_denys_send_email' );
        }
        add_action( 'event_denys_send_email', array( $this,'denys_send_email' ) );
    }

    public function load_plugin_textdomain() {
        load_plugin_textdomain( 'weather', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
    }

    public function action_links( $links ) {
        $links[] = '<a href="admin.php?page=weather">' . __( 'Settings', 'weather' ) . '</a>';

        return $links;
    }

    public function plugin_settings() {

        register_setting( 'weather', 'weather', array( $this, 'sanitize_callback' ) );

        add_settings_section( 'weather', __( 'Main Settings', 'weather' ), '', 'weather' );

        add_settings_field( 'city', __( 'City', 'weather' ), array( $this, 'form_city' ), 'weather', 'weather' );
        add_settings_field( 'email', __( 'Email', 'weather' ), array( $this, 'form_email' ), 'weather', 'weather' );
    }

    public function form_city() {
        $val  = get_option( 'weather' );
        $val  = $val ? $val[ 'city' ] : null;
        $city = array(
            '706483' => __( 'Kharkiv', 'weather' ),
            '706515' => __( 'Kerleut', 'weather' ),
            '706524' => __( 'Kerch', 'weather' ),
            '706546' => __( 'Kehychivka', 'weather' ),
            '706571' => __( 'Kozyatyn', 'weather' ),
        );
        ?>
        <select name="weather[city]">
            <?php foreach ( $city as $key => $value ) { ?>
            <option value="<?php echo $key; ?>" <?php selected( $val, $key ); ?>><?php echo $value;?></option>         
            <?php } ?>            
        </select>
        <?php
    }

    public function form_email( $val ) {
        $val = get_option( 'weather' );
        $val = $val ? $val[ 'email' ] : '';
        ?>
        <input type="email" name="weather[email]" value="<?php echo $val ?>"/>
        <?php
    }

    public function sanitize_callback( $options ) {

        foreach ( $options as $name => & $val ) {
            if ( $name == 'email' )
                $val = strip_tags( $val );

            if ( $name == 'city' )
                $val = intval( $val );
        }

        return $options;
    }

    public function admin_menu_add_external_links_as_submenu() {
        add_menu_page(
        esc_html__( 'Weather', 'weather' ),
                    esc_html__( 'Weather', 'weather' ),
                                'read',
                                'weather',
                                array( $this, 'options_page' ),
                                'dashicons-shield',
                                99
        );
        register_setting( 'weather', 'weather', $sanitize_callback );
    }

    public function options_page() {
        ?>
        <div class="wrap">
            <h1><?php esc_html_e( 'Weather settings', 'weather' ); ?></h1>
            <form id="weather_form" method="post" action="options.php">                
                <?php
                settings_fields( 'weather' );
                do_settings_sections( 'weather' );
                submit_button();
                ?>
            </form>            
        </div>
        <?php
    }
    public function three_hour( $interval ) {
        $interval['three_hour'] = array(
		'interval' => 3 * HOUR_IN_SECONDS, 
		'display' => '3 hour',
	);
        return $interval;
    }
    
    public function denys_send_email() {
        $val = get_option( 'weather' );
        $email = $val['email'];
        $city = $val['city'];
        $api = json_decode(file_get_contents("http://api.openweathermap.org/data/2.5/weather?id=$city&appid=4a486bae928681266692d39b1a7fcc52"));
        if( !empty($api)){
            $temp = $api->main->temp;
            $humidity = $api->main->humidity;
            $msg = "City: $city, temp: $temp, humidity: $humidity";
            wp_mail( $email, '', $msg );
        }
    }

}

function DenysWeather() {
    return DenysWeather::instance();
}

DenysWeather();
